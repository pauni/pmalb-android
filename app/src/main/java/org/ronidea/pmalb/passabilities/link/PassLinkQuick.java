package org.ronidea.pmalb.passabilities.link;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import org.ronidea.pmalb.core.PmalbTransceiver;
import org.ronidea.pmalb.core.models.DeviceManager;

import static org.ronidea.pmalb.core.Protocol.*;



public class PassLinkQuick extends Activity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("PassLinkQuick", "I was called");
        PmalbTransceiver.send(getIntent().getStringExtra(Intent.EXTRA_TEXT).getBytes(),
                TYPE_LINK, this,
                DeviceManager.Selection.get());
        finish();
    }
}
