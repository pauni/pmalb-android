package org.ronidea.pmalb.passabilities.mouse;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import org.ronidea.pmalb.R;
import org.ronidea.pmalb.core.PmalbTransceiver;
import org.ronidea.pmalb.core.Protocol;
import org.ronidea.pmalb.core.models.DeviceManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.net.SocketException;
import java.util.Locale;

import static org.ronidea.pmalb.core.Protocol.*;

public class TouchpadActivity extends AppCompatActivity implements View.OnTouchListener{
    TextView tv_debug;
    ImageView id_cursor;
    PipedOutputStream pipeOut;   // send mouse events to transmitter thread
    PipedInputStream pipeIn;     // read mouse events from ui thread
    Thread transmitter;          // transmit mouse events to desktop
    BufferedReader pipeReader;
    PrintWriter pipeWriter;

    float oldX = 0;
    float oldY = 0;
    float newX = 0;
    float newY = 0;
    float dx = 0;
    float dy = 0;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touchpad);
        tv_debug = findViewById(R.id.tv_debug);
        id_cursor = findViewById(R.id.id_cursor);

        findViewById(R.id.fl_touchpad).setOnTouchListener(this);
        startTransmitter();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch(motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                oldX = motionEvent.getRawX();
                oldY = motionEvent.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                newX = motionEvent.getRawX();
                newY = motionEvent.getRawY();
                dx = newX - oldX;
                dy = newY - oldY;

                try {
                    String mouseData = new JSONObject()
                            .put("move_x", dx)
                            .put("move_y", dy).toString();

                    pipeWriter.println(mouseData);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                oldX = newX;
                oldY = newY;
                tv_debug.setText(String.format(Locale.ENGLISH, "x: %f, y: %f\ndx: %f, dy: %f", newX, newY, dx, dy));

                // small cursor for debugging purposes
                float newx = id_cursor.getX() + dx;
                float newy = id_cursor.getY() + dy;
                if(newx>=0) {
                    id_cursor.setX(newx);
                } else
                    id_cursor.setX(0);

                if (newy>=0) {
                    id_cursor.setY(newy);
                } else
                    id_cursor.setY(0);
                break;
        }
        return true;
    }

    private void startTransmitter() {
        try {
            pipeIn = new PipedInputStream();
            pipeOut = new PipedOutputStream(pipeIn);
            pipeReader = new BufferedReader(new InputStreamReader(pipeIn));
            pipeWriter = new PrintWriter(pipeOut, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        transmitter = new Thread(new Runnable() {
            @Override
            public void run() {
                String data;
                try {
                    while (true) {
                         data = pipeReader.readLine();

                         if (data == null)
                             Log.i("Transmitter", "ITS NULL");
                         else
                             PmalbTransceiver.sendOverUDP(data, TYPE_MOUSE, Protocol.UDP_FLAG_NONE, DeviceManager.Selection.get());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        transmitter.start();
    }
}
