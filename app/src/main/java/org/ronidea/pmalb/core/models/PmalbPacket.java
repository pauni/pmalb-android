package org.ronidea.pmalb.core.models;

/**
 *  PmalbPacket represents data being sent over ethernet, having either one of the
 *  headers defined by the Protocol.
 *  This class bundles the flags, data, used protocol version etc. into a class, making it
 *  easier to access it and pass it around.
 *
 *  NOTE: this class could be improved by a lot, but I'm kinda lazy.. I mean busy*
 *  that could be done in the future:
 *  - make class abstract or st.,
 *  - create 2 subclasses to differentiate between UDP/TCP packets,
 *  - use enum for encryption type,
 *  - remove ipAddr var
 */

public class PmalbPacket {
    // protocol version, data type
    public short version = -1;
    public short type = -1;

    // flags
    public byte encryptionType = -1;
    public boolean isMulticast = false;
    public boolean isBinaryData = false;

    public long payloadSize = -1;
    public byte[] data = null;
    public String ipAddr = null; // TODO: try remove this var, refactor code using it

    public void setFlags(short flags) {
        // by now, i need to know if UDP or TCP...
    }

}
