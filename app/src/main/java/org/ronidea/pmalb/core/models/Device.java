package org.ronidea.pmalb.core.models;

import android.util.Base64;

import static org.ronidea.pmalb.core.Protocol.*;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Arrays;


public class Device {
    private PublicPmalbKey pPK;
    private String ipAddr;
    private String name;
    private String id;

    public Device(byte[] pPKBytes, String ipaddr, String name, String id) {
        this.pPK = new PublicPmalbKey(pPKBytes);
        this.ipAddr = ipaddr;
        this.name = name;
        this.id = id;
    }

    public Device(JSONObject pairingRequest, String ipAddr) throws JSONException {
        this.pPK = new PublicPmalbKey(Base64.decode(pairingRequest.getString(DEVICE_PUBKEY), Base64.DEFAULT));
        this.ipAddr = ipAddr;
        this.name = pairingRequest.getString(DEVICE_NAME);
        this.id = pairingRequest.getString(DEVICE_ID);
    }

    public void setpPK(PublicPmalbKey pPK) {
        this.pPK = pPK;
    }

    public PublicPmalbKey getPPK() {
        return pPK;
    }


    public byte[] getFingerprint() {
        return Arrays.copyOfRange(pPK.getBytes(), 0, 8);
    }


    public void setIpAddr(String ipaddr) {
        this.ipAddr = ipaddr;
    }

    public String getIpAddr() {
        return ipAddr;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
