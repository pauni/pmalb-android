package org.ronidea.pmalb.core.utilities;

import android.util.Log;

import org.libsodium.jni.NaCl;
import org.libsodium.jni.Sodium;
import org.ronidea.pmalb.core.models.DeviceManager;
import org.ronidea.pmalb.core.models.PublicPmalbKey;
import org.ronidea.pmalb.core.models.SecretPmalbKey;

public class Cryptography {
    private static Sodium sodium = NaCl.sodium();

    public static byte[] encrypt(byte[] sourcePlain, PublicPmalbKey remotePublicKey, byte[] nonce) {
        byte[] cipher = new byte[Sodium.crypto_box_macbytes() + sourcePlain.length];
        if (remotePublicKey == null || remotePublicKey.getBoxKey() == null || DeviceManager.Self.getSPK() == null || nonce == null)
            throw new IllegalArgumentException("remotePublicKey or it's content is null");

        if (Sodium.crypto_box_easy(cipher, sourcePlain, sourcePlain.length,
                nonce, remotePublicKey.getBoxKey(), DeviceManager.Self.getSPK().getBoxKey()) != 0) {
            return null;
        }

        Log.i("encrypt","Encrypted using Key: " + Utils.bytohex(remotePublicKey.getBoxKey()));
        return cipher;
    }

    public static byte[] decrypt(byte[] cipher, int msg_len, PublicPmalbKey remotePublicKey, byte[] nonce) {
        byte[] decrypted = new byte[msg_len];

        if (Sodium.crypto_box_open_easy(decrypted, cipher, cipher.length,
                nonce, remotePublicKey.getBoxKey(), DeviceManager.Self.getSPK().getBoxKey()) != 0) {
            return null;
        }

        return decrypted;
    }

    public static byte[] sign(byte[] msg) {
        byte[] signature = new byte[Sodium.crypto_sign_bytes()];
        Sodium.crypto_sign_detached(signature, new int[] {}, msg, msg.length, DeviceManager.Self.getSPK().getSignKey());
        return signature;
    }

    public static boolean verify(byte[] sig, byte[] msg, PublicPmalbKey key) {
        return 0 == Sodium.crypto_sign_verify_detached(sig, msg, msg.length, key.getSignKey());
    }

    // only call on first app start
    public static Object[] generateKeypair() {
        if (DeviceManager.Self.getPPK() == null && DeviceManager.Self.getSPK() == null) {
            Log.i("generatePublicKeypair", "generating new keypair" + Sodium.crypto_box_publickeybytes());

            // Public-key authenticated encryption
            byte[] publicBoxKey = new byte[Sodium.crypto_box_publickeybytes()];
            byte[] secretBoxKey = new byte[Sodium.crypto_box_secretkeybytes()];
            Sodium.crypto_box_keypair(publicBoxKey, secretBoxKey);
            // Public-key signatures
            byte[] publicSignKey = new byte[Sodium.crypto_sign_publickeybytes()];
            byte[] secretSignKey = new byte[Sodium.crypto_sign_secretkeybytes()];
            Sodium.crypto_sign_keypair(publicSignKey, secretSignKey);

            // Ouch..
            return new Object[]{new PublicPmalbKey(publicBoxKey, publicSignKey), new SecretPmalbKey(secretBoxKey, secretSignKey)};
        } else throw new RuntimeException("Attempt to generate new keypair, overriding old one!");
    }


}

