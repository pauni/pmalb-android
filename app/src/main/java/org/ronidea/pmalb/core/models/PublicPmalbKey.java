package org.ronidea.pmalb.core.models;

import org.libsodium.jni.NaCl;
import org.libsodium.jni.Sodium;

import java.util.Arrays;

public class PublicPmalbKey {
    private static Sodium sodium = NaCl.sodium();

    private byte[] publicBoxKey;
    private byte[] publicSignKey;

    public PublicPmalbKey(byte[] publicBoxKey, byte[] publicSignKeys) {
        this.publicBoxKey = publicBoxKey;
        this.publicSignKey = publicSignKeys;
    }

    public PublicPmalbKey(byte[] pmalbKeyBytes) {
        publicSignKey = Arrays.copyOfRange(pmalbKeyBytes, 0, Sodium.crypto_sign_publickeybytes());
        publicBoxKey = Arrays.copyOfRange(pmalbKeyBytes, Sodium.crypto_sign_publickeybytes(), pmalbKeyBytes.length);
    }

    public byte[] getBoxKey() {
        return publicBoxKey;
    }

    public byte[] getSignKey() {
        return publicSignKey;
    }

    public byte[] getBytes() {
        byte[] result = Arrays.copyOf(publicSignKey, publicSignKey.length + publicBoxKey.length);
        System.arraycopy(publicBoxKey, 0, result, publicSignKey.length, publicBoxKey.length);
        return result;
    }
}
