package org.ronidea.pmalb.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import org.ronidea.pmalb.core.models.DeviceManager;


public class IpAddrChangedListener extends BroadcastReceiver {
    String currentIpAddr;

    Context context;
    IntentFilter filter = new IntentFilter();

    public IpAddrChangedListener(Context context) {
        this.context = context;
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        currentIpAddr = DeviceManager.Self.getIpAddr(context);
    }

    void start(Context context) {
        context.registerReceiver(this, filter);
    }

    void stop(Context context) {
        context.unregisterReceiver(this);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // if ip address has changed, go tell everyone!
        NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        if (!info.isConnected()) {
            return;
        }

        String newIpAddr = DeviceManager.Self.getIpAddr(context);
        if (newIpAddr != null && !newIpAddr.equals(currentIpAddr)) {
            Log.i("IPlistener", "I've got new IP. Go tell everyone!");
            PmalbTransceiver.sendDiscoBeacon();
        }

        currentIpAddr = newIpAddr; // HOW COULD I FORGET THAT?! >:-(
    }

}
