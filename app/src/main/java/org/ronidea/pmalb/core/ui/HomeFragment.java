package org.ronidea.pmalb.core.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewSwitcher;

import com.google.zxing.integration.android.IntentIntegrator;

import org.ronidea.pmalb.R;
import org.ronidea.pmalb.core.PmalbTransceiver;
import org.ronidea.pmalb.core.QRCodeActivity;

public class HomeFragment extends Fragment {
    SwitchCompat sw_main;
    ImageSwitcher is_logo;
    Context context;
    LinearLayout ll_banner;
    FloatingActionButton fab;
    Snackbar pmalbDisabledSnackbar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        sw_main = root.findViewById(R.id.sw_main);
        is_logo = root.findViewById(R.id.is_logo);
        ll_banner = root.findViewById(R.id.ll_banner);
        fab = super.getActivity().findViewById(R.id.fab_pairing);

        pmalbDisabledSnackbar = Snackbar.make(fab, "Pmalb must be enabled", Snackbar.LENGTH_LONG);
        pmalbDisabledSnackbar.setAction("enable", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sw_main.setChecked(true);
            }
        });

        setFabEnabled(false);

        if (PmalbTransceiver.isActive()) {
            sw_main.setChecked(true);
        }

        is_logo.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView view = new ImageView(getContext());
                view.setScaleType(ImageView.ScaleType.FIT_CENTER);
                view.setLayoutParams(new ImageSwitcher.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                return view;
            }
        });

        Animation in = AnimationUtils.loadAnimation(getContext(), R.anim.flip_in);
        Animation out = AnimationUtils.loadAnimation(getContext(), R.anim.flip_out);

        is_logo.setImageResource(R.drawable.ic_launcher);
        is_logo.setInAnimation(in);
        is_logo.setOutAnimation(out);

        registerListeners();
        return root;
    }

    private void setFabEnabled(boolean enabled) {
        if (enabled) {
            fab.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
            // fab.setOnClickListener(null);
        } else {
            fab.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.darker_gray)));
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!PmalbTransceiver.isActive())
                        pmalbDisabledSnackbar.show();
                    else
                        scanQRCode();
                }
            });
        }
    }

    public void scanQRCode() {
        PmalbTransceiver.setPairingMode(true);

        // TODO: debug this casting
        IntentIntegrator integrator = new IntentIntegrator(getActivity());
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Pair Computer");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(QRCodeActivity.class);
        integrator.initiateScan();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void registerListeners() {
        sw_main.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b && !PmalbTransceiver.isActive()) {
                    context.startService(new Intent(context, PmalbTransceiver.class));
                    setFabEnabled(true);
                    is_logo.setImageResource(R.drawable.ic_launcher);
                } else {
                    context.stopService(new Intent(context, PmalbTransceiver.class));
                    setFabEnabled(false);
                    is_logo.setImageResource(R.drawable.ic_launcher_disabled);
                }
            }
        });

        ll_banner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    sw_main.setChecked(!PmalbTransceiver.isActive());
                }
                return false;
            }


        });

    }

    @Override
    public void onAttach(Context context) {
        this.context = context;
        super.onAttach(context);
    }


}
