package org.ronidea.pmalb.core.models;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.ronidea.pmalb.core.utilities.Cryptography;
import org.ronidea.pmalb.core.utilities.Prefs;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static android.content.Context.WIFI_SERVICE;
import static org.ronidea.pmalb.core.Protocol.DEVICE_ID;
import static org.ronidea.pmalb.core.Protocol.DEVICE_NAME;
import static org.ronidea.pmalb.core.Protocol.DEVICE_PUBKEY;

/**
 *  DeviceManager and it's subclasses are holding a list for paired, selected and
 *  discovered devices. Its providing add(), remove(), get() methods to manipulate
 *  the lists and/or retrieve them. Whenever a pairing, selection or discovery occurs,
 *  the DeviceManager must be called accordingly!
 *
 *  NOTE:
 *  If a discovery packet comes from a paired device, the Discovery subclass will automatically
 *  update the respective entry in the Paired subclass, so no manual outside check required.
 *
 *  DeviceManager (and it's subclasses) must only be initialized once.
 */

public class DeviceManager {

    /**
     * Called on first app start
     */
    public static void init(Context context) {
        Self.init(context);
        Paired.init();
        Selection.init();
    }

    /**
     * Called on app start
     */
    public static void load() {
        Self.load();
        Paired.load();
        Selection.load();
    }



    /**
     *  Holds the list of selected devices
     */
    public static class Paired {
        private static HashMap<byte[], Device> paired;

        static void init() {
            paired = new HashMap<>();
            Log.i("DeviceManager.Paired", "init()");
        }

        private static void load() {
            paired = new HashMap<>();
            Device[] arrPaired = Prefs.loadPairedDevices();

            if (arrPaired != null) {
                for (Device d : arrPaired) {
                    if (d != null)
                        paired.put(d.getFingerprint(), d);
                }
            }
        }

        private static void save() {
            Prefs.savePairedDevices(paired.values().toArray(new Device[0]));
        }

        public static void add(@NonNull Device device) {
            paired.remove(device.getFingerprint());
            paired.put(device.getFingerprint(), device);
            save();
        }

        public static void remove(Device device) {
            paired.remove(device.getFingerprint());
        }

        static void update(@NonNull Device device) {
            Log.i("DeviceManager", "update: " + device.getName());
            Device d = paired.get(device.getFingerprint());



            if (device.getIpAddr() != null) {
                d.setIpAddr(device.getIpAddr());
            }


            if (device.getPPK() != null) {
                d.setpPK(device.getPPK());
            }
        }

        public static Device[] get() {
            return paired.values().toArray(new Device[0]);
        }

        public static Device getByIp(String addr) {
            for (Device d : paired.values()) {
                if (d.getIpAddr().equals(addr))
                    return d;
            }
            return null;
        }
    }

    /**
     *  Holds the list of devices, which are to receive data
     */
    public static class Selection {
        private static Set<String> identifiers;   // holding IDs of selected devices

        private static void init() {
            identifiers = new HashSet<>();

            save();
            Log.i("DeviceManager.Selection", "init()");
        }

        private static void load() {
            String[] arrSelected = Prefs.loadSelectedDevices();

            if (arrSelected == null) {
                Log.e("DeviceManager.Selection", "arrSelected == null;");
                return;
            }

            identifiers = new HashSet<>(Arrays.asList(arrSelected));
        }

        private static void save() {
            Prefs.saveSelectedDevices(identifiers.toArray(new String[0]));
        }

        public static void add(Device device) {
            identifiers.remove(device.getId());
            identifiers.add(device.getId());
        }

        public static void remove(Device device) {
            identifiers.remove(device.getId());
        }

        public static Device[] get() {
            Set<String> selectionCopy = new HashSet<>(identifiers);
            List<Device> selectedDevices = new ArrayList<>();
            for (Device d : Paired.get()) {
                for (String id : selectionCopy) {
                    if (d.getId().equals(id)) {
                        selectedDevices.add(d);
                        selectionCopy.remove(id);
                    }
                }
            }
            return selectedDevices.toArray(new Device[0]);
        }


    }

    /**
     *  Holds the list of discovered devices (temporary)
     */
    public static class Discovery {
        // value=disco device; key=their fingerprint // TODO: test if SparseArray works well
        private static HashMap<byte[], Device> discoveries = new HashMap<>();

        /**
         * Adds or updates a device to the discoveries list. If the device is a
         * paired device, its entry gets updated in subclass 'Paired' instead.
         * @param device Discovered device to be added to the list
         * @param validity Result of the Cryptography.verify() method
         * @return Returns true if device was added/updated. Returns false if device was a
         * paired device, meaning respective entry in subclass 'Paired' was updated instead
         */
        public static boolean add(Device device, boolean validity) {
            discoveries.remove(device.getFingerprint());
            Log.i("DevMngr", ""+discoveries.size());

            // if it's a paired device, check for validity and update the respective entry
            if (Paired.paired.containsKey(device.getFingerprint()) && validity) {
                Paired.update(device);
                return false;
            } else {
                discoveries.put(device.getFingerprint(), device);
                return true;
            }
        }

        static void remove(Device device) {
            discoveries.remove(device.getFingerprint());
        }

        public static Device[] get() {
            return discoveries.values().toArray(new Device[0]);
        }

        public static Device get(int fingerprint) {
            return discoveries.get(fingerprint);
        }
    }

    /**
     *  Holds the information about the own device
     */
    public static class Self {
        private static final String KEY_SPK = "secret_key";
        private static String name;
        private static String id;
        private static PublicPmalbKey pPK;
        private static SecretPmalbKey sPK;

        private static void init(Context context) {
            Log.i("DeviceManager.Self", "init()");

            Object[] keypair = Cryptography.generateKeypair();
            pPK = (PublicPmalbKey) keypair[0];
            sPK = (SecretPmalbKey) keypair[1];
            name =  Settings.Secure.getString(context.getContentResolver(), "bluetooth_name");
            id = "PETER";
            save();

            Log.i("DeviceManager.Self", "init(): length ppk/spk: "+pPK.getBytes().length +"/"+sPK.getBytes().length);

        }

        private static void save() {
            JSONObject o = Self.toJson();
            if (o != null) {
                Prefs.saveSelf(o.toString());
            }
        }

        private static void load() {
            JSONObject jsonSelf = Prefs.loadSelf();
            if (jsonSelf == null) {
                Log.e("DeviceManager.Selection", "jsonSelf == null;");
                return;
            }

            try {
                pPK = new PublicPmalbKey(Base64.decode(jsonSelf.getString(DEVICE_PUBKEY), Base64.DEFAULT));
                sPK = new SecretPmalbKey(Base64.decode(jsonSelf.getString(KEY_SPK), Base64.DEFAULT));
                name = jsonSelf.getString(DEVICE_NAME);
                id = jsonSelf.getString(DEVICE_ID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.i("DeviceManager.Self", "load(): length ppk/spk: "+pPK.getBytes().length +"/"+sPK.getBytes().length);
        }

        /**
         * Delete pub and sec key, so new pair can be generated.
         * Called ONLY by test class
         */
        public static void resetKeys(String auth) {
            if (auth.equals("test")) {
                pPK = null;
                sPK = null;
            }
        }

        public static String getIpAddr(Context context) {
            WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
            if (wifiManager == null) {
                Log.i("Utils", "wifiManager == null true");
                return null;
            }

            int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
            // Convert little-endian to big-endian if needed
            if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
                ipAddress = Integer.reverseBytes(ipAddress);
            }
            byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

            String ipAddressString;
            try {
                ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
            } catch (UnknownHostException ex) {
                Log.e("WIFIIP", "Unable to get host address.");
                ipAddressString = null;
            }
            return ipAddressString;
        }

        public static PublicPmalbKey getPPK() {
            return pPK;
        }

        public static SecretPmalbKey getSPK() {
            return sPK;
        }

        public static String getName() {
            return name;
        }

        public static String getId() {
            return id;
        }

        public static JSONObject toJson() {
            try {
                JSONObject jsonSelf = new JSONObject();
                jsonSelf.put(DEVICE_NAME,   name);
                jsonSelf.put(DEVICE_ID,     id);
                jsonSelf.put(DEVICE_PUBKEY, Base64.encodeToString(pPK.getBytes(), Base64.DEFAULT));
                jsonSelf.put(KEY_SPK, Base64.encodeToString(sPK.getBytes(), Base64.DEFAULT));
                return jsonSelf;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
