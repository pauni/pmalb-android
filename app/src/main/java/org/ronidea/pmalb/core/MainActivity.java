package org.ronidea.pmalb.core;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.libsodium.jni.NaCl;
import org.libsodium.jni.Sodium;
import org.ronidea.pmalb.R;
import org.ronidea.pmalb.core.models.Device;
import org.ronidea.pmalb.core.models.DeviceManager;
import org.ronidea.pmalb.core.ui.PairingResultActivity;
import org.ronidea.pmalb.core.utilities.Prefs;
import org.ronidea.pmalb.core.utilities.Utils;
import org.ronidea.pmalb.passabilities.mouse.TouchpadActivity;

import java.util.Arrays;


public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_PAIRING = 1337; // oh man...
    public static String cryptoInfo;

    private AppBarConfiguration mAppBarConfiguration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_home, R.id.nav_paired)
                .setDrawerLayout(drawer).build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        initializePmalb();
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void initializePmalb() {
        NaCl.sodium();
        Sodium.sodium_init();
        Prefs.init(this);

        if (Prefs.isFirstStart())
            onFirstStart();
        else
            DeviceManager.load();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("MainActivity", "onResume");

        if (PmalbTransceiver.isActive()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    // just always call it, the method will test if its even activated
                    // clear scan history
                    PmalbTransceiver.setPairingMode(false);
                    QRCodeActivity.setLatestSecretNumber(-1);
                }
            }, 1000);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("onActivityResult", "" + requestCode + " " + resultCode);
        Toast.makeText(this, "" + requestCode + " " + resultCode, Toast.LENGTH_SHORT).show();

        // Activity result from PairingResultActivity
        if (requestCode == REQUEST_PAIRING && resultCode == 1) {
                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.nav_paired);
        } else if (IntentIntegrator.REQUEST_CODE == requestCode) {
            // Activity result from QRCodeScanActivity
            startActivityForResult(new Intent(this, PairingResultActivity.class), REQUEST_PAIRING);
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

            if(result == null || result.getContents() == null) {
                Log.i("onActivityResult", "Scan result is null");
                return;
            }

            try {
                byte[] scan = Utils.hextoby(result.getContents());
                long secretNumber = Protocol.bytesToInt(Arrays.copyOfRange(scan, 0, 4));
                byte[] fingerprint = Arrays.copyOfRange(scan, 4, 12);
                QRCodeActivity.setLatestSecretNumber(secretNumber);
                Device candidate = DeviceManager.Discovery.get(Arrays.hashCode(fingerprint));

                if (candidate == null) {
                    Log.i("onActivityResult", "candidate is null");
                    return;
                }

                PmalbTransceiver.send(Protocol.createPairingRequest(secretNumber), Protocol.TYPE_PAIRING, getApplication(), candidate);
                Toast.makeText(this,"secret: " + secretNumber + "\nfingerprint: " + Utils.bytohex(fingerprint), Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "OMG what fail!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void onFirstStart() {
        DeviceManager.init(this);
    }

    public void debugInfo(View v) throws JSONException {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        StringBuilder text = new StringBuilder(
                "Self.getIpAddr(): " + DeviceManager.Self.getIpAddr(this) + "\n\n" +
                        "Self.toJson():\n" + DeviceManager.Self.toJson().toString(4) + "\n\n" +
                        "Paired devices: " + DeviceManager.Paired.get().length + "\n\n" +
                        "Selected devices: " + DeviceManager.Selection.get().length + "\n\n" +
                        "Discovered devices: " + DeviceManager.Discovery.get().length + "\n");

        for (Device d : DeviceManager.Discovery.get()) {
            text.append(("IP: ")).append(d.getIpAddr()).append("\nPubkkey: ").append(Utils.bytohex(d.getPPK().getBytes())).append("\n\n");
        }

        try {
            text.append("DiscoveryPacket: ").append(Utils.bytohex(Protocol.createDiscoBeacon())).append("\n")
                    .append("length: ").append(Protocol.createDiscoBeacon().length);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        builder.setMessage(text.toString());
        builder.create();
        builder.show();
    }

    public void testTouchpad(View v) {
        startActivity(new Intent(this, TouchpadActivity.class));
    }


}
