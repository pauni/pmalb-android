package org.ronidea.pmalb.core;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;

import org.libsodium.jni.Sodium;
import org.libsodium.jni.SodiumConstants;


import org.ronidea.pmalb.R;
import org.ronidea.pmalb.core.models.Device;
import org.ronidea.pmalb.core.models.DeviceManager;
import org.ronidea.pmalb.core.models.PmalbPacket;
import org.ronidea.pmalb.core.utilities.Cryptography;
import org.ronidea.pmalb.core.utilities.Prefs;
import org.ronidea.pmalb.core.utilities.Utils;

import static org.ronidea.pmalb.core.Protocol.*;

/**
 *  Pmalb server
 *  TCP socket
 *  UDP socket
 *  BG Service
 *
 *  Basically: handles all the network stuff.
 *  The TCP server is used for regular data, such as files, urls, multimedia controls, while
 *  the UDP server is designed to receive discovery beacons (multicast) and mouse-events (unicast)
 *
 *
 */

public class PmalbTransceiver extends Service {
    private static boolean isActive = false;
    private static boolean loadingComplete = false;
    private static boolean allowPairing = false; // additionally, sends lots of disco beacons

    public static final String EXTRA_MSGR_PAIRING_RESULT = "messenger_pairing_result";

    private static Thread discoFlood;
    private static Handler networkHandler; // handler to execute code AFTER launch is complete. TODO: use it when queueing is required

    private static ServerSocket server;        // Pmalb server (receives from desktop)
    private static Socket serverClient;        // a client connected to Pmalb server
    private static DataInputStream serverIn;   // input coming to the server

    private static Socket client;              // Pmalb client - sent data to desktop
    private static DataOutputStream clientOut; // Output stream of Pmalb client
    private static BufferedReader clientIn;    // Input stream for server responses to Pmalb client
    private static MulticastSocket dgSocket;    //

    private IpAddrChangedListener addrChangedListener;
    private static InetAddress group;




    /**
     * Overrides
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        isActive = true;
        prepareHandler(); // block networkHandler until everything in this method is set up properly.
        DeviceManager.load(); // prevent NPE

        try {
            group = Inet6Address.getByName(MULTICAST_GROUP);

            server = new ServerSocket();
            server.setReuseAddress(true);
            server.bind(new InetSocketAddress(PORT));

            dgSocket = new MulticastSocket(PORT);
            dgSocket.setNetworkInterface(NetworkInterface.getByName("wlan0"));
            dgSocket.setLoopbackMode(false);
            dgSocket.setReuseAddress(true);
            dgSocket.joinGroup(new InetSocketAddress(group, PORT), NetworkInterface.getByName("wlan0"));
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: implement local broadcast, telling MainActivity service couldn't start
            stopSelf();
            isActive = false;
            return START_NOT_STICKY;
        }

        addrChangedListener = new IpAddrChangedListener(this);
        showForegroundNotification();
        startTCPServer();
        startUDPServer();
        addrChangedListener.start(this);

        Toast.makeText(this, "Pmalb is active in the background", Toast.LENGTH_SHORT).show();
        sendDiscoBeacon();

        loadingComplete = true;

        discoFlood = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (PmalbTransceiver.isPairingMode()) {
                        sendDiscoBeacon();
                        Thread.sleep(3000);
                    }
                    Log.i("discoFlood", "EXIT DISCO FLOOD! :(");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i("ReceiverService", "onDestroy()");
        PmalbTransceiver.closeServerSockets();
        addrChangedListener.stop(this);
        isActive = false;
        super.onDestroy();
    }



    private void prepareHandler() {
        networkHandler = new Handler();
        networkHandler.post(new Runnable() {
            @Override
            public void run() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("prepareHandler", "blocking handler");
                        // sleep and block, until loading of server stuff completed
                        while (!loadingComplete) {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.i("prepareHandler", "releasing handler");
                    }
                }).start();
            }
        });
    }

    /**
     *  Deserialize the input stream and process payload, according to it's type.
     *  Discovery beacons and pairing requests are being passed to DeviceManager
     */
    private void startTCPServer() {
        Log.i("PmalbTransceiver", "startTCPServer()");
        new Thread(new Runnable() {
            @Override
            public void run() {
                PmalbPacket pp;
                byte[] buf_header = new byte[HEADER_LEN];

                try {
                    while (isActive) {
                        Log.i("startTCPServer", "waiting for clients...");
                        serverClient = server.accept();
                        serverIn = new DataInputStream(serverClient.getInputStream());

                        // TODO: Handle msg smaller than HEADER_LEN
                        if (serverIn.read(buf_header, 0, HEADER_LEN) == -1) {
                            Log.e("startTCPServer()", "serverIn.read() == -1");
                            continue;
                        }

                        try {
                            pp = Protocol.deserializeFromTcp(buf_header);
                        } catch (ArrayIndexOutOfBoundsException e) {
                            Log.e("startTCPServer()", "Failed to deserializeFromTcp header");
                            e.printStackTrace();
                            continue;
                        }

                        if (pp.payloadSize < Integer.MAX_VALUE) {
                            processPmalbPacket(pp, getApplicationContext());
                        }

                        serverIn.close();
                        serverClient.close();
                    }
                } catch (Exception e) {
                    Log.e("startTCPServer()", "SERVER CRASHED");
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void startUDPServer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] buf;
                DatagramPacket dgPacket;
                PmalbPacket packet;

                try {
                    while (isActive) {
                        Log.i("UDPServer", "listening...");
                        buf  = new byte[64*1024];
                        dgPacket = new DatagramPacket(buf, 64*1024);
                        dgSocket.receive(dgPacket);
                        Log.i("UDPServer", "received " + dgPacket.getLength() + " (-4) bytes");
                        packet = Protocol.deserializeFromUdp(
                                Arrays.copyOfRange(dgPacket.getData(), 0, dgPacket.getLength()));

                        if (packet == null) {
                            Log.e("UDPServer", "packet == null");
                            continue;
                        }

                        packet.ipAddr = dgPacket.getAddress().getHostAddress();
                        processQuickPacket(packet);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    // TODO: use processPmalbPacket in Protocol.java instead of processQuickPacket
    private static void processQuickPacket(PmalbPacket packet) {
        switch (packet.type) {
            case TYPE_DISCO_BEACON:
                // bytes for signature and key are defined in the documentation
                byte[] sig = Arrays.copyOfRange(packet.data, 0, Sodium.crypto_sign_bytes());
                byte[] key = Arrays.copyOfRange(packet.data, Sodium.crypto_sign_bytes(), packet.data.length);
                final Device device = new Device(key, packet.ipAddr, "PAUL", "PETER");

                // add device to discovery-list. If it's a paired device, send disco response
                if (!DeviceManager.Discovery.add(device, Cryptography.verify(sig, key, device.getPPK()))) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            sendOverUDP(Protocol.createDiscoBeacon(), TYPE_DISCO_BEACON,
                                    UDP_FLAG_UNENCRYPTED | UDP_FLAG_UNICAST, device);
                            Log.i("processQuickPacket", "sending disco-beacon-response");
                        }
                    }).start();
                }
                break;
        }
    }

    private static void closeServerSockets() {
        // if no serverClient has connected yet, or the on/off toggle
        // is quickly pressed twice, each variable could be uninitialized.
        try {
            if (serverIn != null)
                serverIn.close();

            if (server != null)
                server.close();

            if (serverClient != null)
                serverClient.close();

            if (dgSocket != null)
                dgSocket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    /**
     * Send stuff to the desktop (TCP) [default]
     *
     * @param data The data to be sent to the selected desktop.
     * @param type Valid types are defined in the Protocol class.
     * @param destinations An array containing the device(s) the data should be send to
     */
    public static void send(final byte[] data, final short type, final Context context, final Device... destinations) {
        // TODO: implement SQLite to store multiple desktops. Choose in share activity
        new Thread(new Runnable() {
            @Override
            public void run() {
                // send to all selected devices
                for (Device d : destinations) {
                    try {
                        // connect to server
                        client = new Socket(d.getIpAddr(), PORT);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }

                    try {
                        // encrypt, add header, send data
                        clientOut = new DataOutputStream(client.getOutputStream());
                        byte[] cipher = Cryptography.encrypt(data, d.getPPK(), new byte[SodiumConstants.NONCE_BYTES]);
                        byte[] output = serializeForTcp(cipher, type);
                        clientOut.write(output);
                        clientOut.close();

                        // ONLY FOR DEBUGGING ENCRYPTION
                        MainActivity.cryptoInfo = "encrypted with (length " + d.getPPK().getBytes().length + ": " + Utils.bytohex(d.getPPK().getBytes())
                                                + "\n\nnonce (length " + (new byte[SodiumConstants.NONCE_BYTES]).length + ":\n" + Utils.bytohex(new byte[SodiumConstants.NONCE_BYTES])
                                                + "\n\nsigned with (length" + Prefs.loadPrivateKey().length + ":\n" + Utils.bytohex(Prefs.loadPrivateKey());


                        Log.i("PmalbTransceiver", "Bytes sent: " + output.length + ", type: " + type);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    /**
     * Send stuff to the desktop (UDP) [Must call from separate thread]
     *
     * @param data The data to be sent to the selected desktop.
     * @param type Valid types are defined in the Protocol class.
     * @param destinations An array containing the device(s) the data should be send to.
     */
    public static void sendOverUDP(final Object data, final short type, final int flags, final Device... destinations) {
        for (Device d : destinations) {
            try {
                byte[] buf = Protocol.serializeForUdp(data, type, flags);
                DatagramPacket packet = new DatagramPacket(buf, buf.length, InetAddress.getByName(d.getIpAddr()), PORT);
                dgSocket.send(packet);
                Log.i("sendOverUDP", "send packet: "+packet.toString());
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("sendOverUDP", "failed to send over UDP");
            }
        }
    }

    /**
     * Sends discovery beacon via UDP multicast to find pmalb-desktops.
     * Called when IP-addr, WLAN-state changed and while in pairing mode
     */
    public static void sendDiscoBeacon() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    NetworkInterface nif = NetworkInterface.getByName("wlan0");
                    //MulticastSocket socket = new MulticastSocket();
                    //socket.setNetworkInterface(nif);
                    byte[] buffer = Protocol.createDiscoBeacon();
                    buffer = Protocol.serializeForUdp(buffer, TYPE_DISCO_BEACON,
                            UDP_FLAG_UNENCRYPTED | UDP_FLAG_MULTICAST);

                    if (buffer == null) {
                        Log.e("sendDiscoBeacon", "buffer == null");
                        return;
                    }

                    InetAddress a = Inet6Address.getByName("fe80::86e4:bfa8:4c2a:83b7");
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    packet.setAddress(group);

                    dgSocket.send(packet);

                    Log.i("sendDiscoBeacon", "sent successful, length "+buffer.length);
                } catch (SocketException e) {
                    Log.e("sendDiscoBeacon", "Couldn't create DatagramSocket");
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    Log.e("sendDiscoBeacon", "Invalid IP Address");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.e("sendDiscoBeacon", "Couldn't send DatagramPacket");
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /** MAYBE UNNECESSARY/REDUNDANT. REPLACE BY USING SEND()?
     *
     *  Sends discovery beacon response to a specific address via UDP unicast.
     *  Call when receiving a disco beacon from a paired device
     */
    public static void sendDiscoResponse(final Device device) {

    }



    /**
     *  The server will only process pairing requests, when allowPairing is set to true
     *  In this state, disco beacons are sent out repeatedly.
     *  Don't forget to disable after pairing!
     */
    public static void setPairingMode(boolean allowPairing) {
        PmalbTransceiver.allowPairing = allowPairing;

        if (allowPairing) {
            discoFlood.start();
        } else if(discoFlood != null && discoFlood.isAlive()) {
            discoFlood.interrupt();
        }

    }



    /**
     * Save large files (binary mostly..) to storage and return file path
     *
     * @param pp The data
     * @param serverIn Input (network) stream, where file can be read from
     */
    private static File savePayloadToPhone(PmalbPacket pp, DataInputStream serverIn) {
        return null;
    }

    private void showForegroundNotification() {
        Notification notification;
        NotificationManager nM = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (nM == null) {
            Log.e("showForeground", "Ahhhhhh");
            return;
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("roomtour.BG",
                    "Enabling background work", NotificationManager.IMPORTANCE_LOW);
            channel.enableLights(false);
            channel.enableVibration(false);
            nM.createNotificationChannel(channel);

            notification = new Notification.Builder(this, "roomtour.BG")
                    .setContentText("Connected")
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentIntent(pendingIntent)
                    .build();
        } else {
            notification = new Notification.Builder(this)
                    .setContentTitle("notification")
                    .setContentText("Connected")
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentIntent(pendingIntent)
                    .build();
        }

        startForeground(5,notification);
    }

    public static boolean isActive() {
        return isActive;
    }

    public static boolean isPairingMode() {
        return allowPairing;
    }

    private void notification() {
        /*((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE))
                .notify(3, new Notification.Builder(this, "roomtour.ALARM_CHANNEL")
                        .setContentTitle(" ")
                        .setSmallIcon(R.drawable.ic_alarm)
                        .build()
                );*/
    }
}
