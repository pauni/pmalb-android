package org.ronidea.pmalb.core.utilities;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ronidea.pmalb.core.models.Device;

import java.util.Random;

/**
 * Store stuff
 */

public class Prefs {
    private static SharedPreferences prefs = null;
    private static SharedPreferences.Editor editor;

    private static final String KEY_SELECTED_DEVICES = "selected_devices";
    private static final String KEY_PAIRED_DEVICES   = "paired_name";
    private static final String KEY_SELF             = "self";
    private static final String KEY_FIRST_START      = "first_start";

    public static void init(Context context) {
        prefs = context.getSharedPreferences("generalPrefs", 0);
    }

    public static boolean isFirstStart() {
        boolean b = prefs.getBoolean(KEY_FIRST_START, true);
        if (b) {
            editor = prefs.edit();
            editor.putBoolean(KEY_FIRST_START, false);
            editor.apply();
        }
        return b;
    }


    /**
     *  Load/save stuff; convenience methods
     */
    public static void savePairedDevices(Device[] devices) {
        try {
            setString(KEY_PAIRED_DEVICES, new JSONArray(devices).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static Device[] loadPairedDevices() {
        try {
            String stringPaired = prefs.getString(KEY_PAIRED_DEVICES, null);
            if (stringPaired == null)
                return null;

            JSONArray jsonPaired = new JSONArray(stringPaired);
            Device[] paired = new Device[jsonPaired.length()];
            for (int i = 0; i < jsonPaired.length(); i++) {
                paired[i] = (Device) jsonPaired.get(i);
            }
            return paired;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void saveSelectedDevices(String[] selection) {
        try {
            setString(KEY_SELECTED_DEVICES, new JSONArray(selection).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static String[] loadSelectedDevices() {
        try {
            String jsonString = prefs.getString(KEY_SELECTED_DEVICES, null);
            if (jsonString == null) {
                Log.i("loadSelectedDevices", "Prefs==null; no device selected");
            }
            JSONArray jsonSelected = new JSONArray(jsonString);
            String[] selection = new String[jsonSelected.length()];
            for (int i = 0; i < jsonSelected.length(); i++) {
                selection[i] = (String) jsonSelected.get(i);
            }
            return selection;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void savePrivateKey(byte[] privateKey) {
        String b64 = Base64.encodeToString(privateKey, Base64.DEFAULT);
        setString("private_key", b64);

    }

    public static byte[] loadPrivateKey() {
        return Base64.decode(prefs.getString("private_key", null), Base64.DEFAULT);
    }

    public static void saveSelf(String jsonString) {
        setString(KEY_SELF, jsonString);

    }

    public static JSONObject loadSelf() {
        String jsonString = prefs.getString(KEY_SELF, null);
        if (jsonString == null) {
            Log.e("Prefs", "loadSelf: jsonString == null;");
            return null;
        }

        try {
            return new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }


    /**
     * Basic SharedPreference operations. Only private access.
     */
    private static void setString(String key, String value) {
        editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
        Log.i("Prefs", "saved: key="+key+" string="+value);
    }

    private static String getString(String key) {
        return prefs.getString(key, null);
    }



    private static void setInt(String key, int value) {
        editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
        Log.i("Prefs", "saved: key="+key+" string="+value);
    }
}
