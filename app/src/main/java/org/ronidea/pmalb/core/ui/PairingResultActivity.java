package org.ronidea.pmalb.core.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.ronidea.pmalb.R;

public class PairingResultActivity extends AppCompatActivity {
    public static int PAIRING_RESPONSE_TIME_TOLERANCE = 1; // IN SECONDS
    public static final String BC_PAIRING_RES = "pairing_result";
    private static boolean resultPending = true;

    private BroadcastReceiver receiver;
    private FrameLayout fl_status;
    private TextView tv_ok;
    private TextView tv_cancel;
    private TextView tv_status;



    /**
     * Let there be dialog!
     * @param savedInstanceState I have absolutely No IdEa :-0
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pairing_result);
        setTitle(getString(R.string.title_pairing_result_dialog));
        init();

        registerReceiver(receiver, new IntentFilter(BC_PAIRING_RES));
        if (resultPending) {
            ProgressBar progressBar = new ProgressBar(this);
            progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            fl_status.addView(progressBar);
        }
        abandonAllHopeAfter(PAIRING_RESPONSE_TIME_TOLERANCE);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    /**
     * If we don't unregister the receiver, their will be rain tomorrow.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    /**
     * Initialize views, listeners and receivers
     */
    private void init() {
        fl_status = findViewById(R.id.fl_status);
        tv_ok = findViewById(R.id.tv_ok);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_status = findViewById(R.id.tv_status);
        tv_ok.setVisibility(View.GONE);

        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setPairingResult(intent.getBooleanExtra(BC_PAIRING_RES, false));
            }
        };
    }

    /**
     * Don't wait for a response anymore.
     * @param pairingResponseTimeTolerance Time in Seconds we want to wait until giving up hope
     */
    private void abandonAllHopeAfter(int pairingResponseTimeTolerance) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sendBroadcast(new Intent(BC_PAIRING_RES).putExtra(BC_PAIRING_RES, false));
            }
        }, pairingResponseTimeTolerance * 1000);
    }

    /**
     * Display result of pairing with a wonderfully descriptive thump icon
     * @param result Successful = true, failed = false
     */
    void setPairingResult(boolean result) {
        resultPending = false;
        ImageView image = new ImageView(this);
        image.setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);

        setResult(result ? 1 : 0);

        if (result) {
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumb_up));
            tv_status.setText(R.string.pairing_result_success);
            tv_cancel.setVisibility(View.INVISIBLE);
            tv_ok.setVisibility(View.VISIBLE);
        } else {
            image.setColorFilter(getResources().getColor(R.color.failure), PorterDuff.Mode.SRC_ATOP);
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumb_down));
            tv_status.setText(R.string.pairing_result_failure);
        }

        fl_status.removeAllViews();
        fl_status.addView(image);
    }
}
