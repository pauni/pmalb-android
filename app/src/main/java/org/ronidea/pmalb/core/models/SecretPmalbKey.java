package org.ronidea.pmalb.core.models;


import android.util.Log;

import org.libsodium.jni.NaCl;
import org.libsodium.jni.Sodium;

import java.util.Arrays;

public class SecretPmalbKey {
    private static Sodium sodium = NaCl.sodium();

    private byte[] secretSignKey;
    private byte[] secretBoxKey;

    public SecretPmalbKey(byte[] privateBoxKey, byte[] privateSignKey) {
        this.secretBoxKey = privateBoxKey;
        this.secretSignKey = privateSignKey;
    }

    public SecretPmalbKey(byte[] pmalbKeyBytes) {
        secretSignKey = Arrays.copyOfRange(pmalbKeyBytes, 0, Sodium.crypto_sign_secretkeybytes());

        secretSignKey = new byte[Sodium.crypto_box_secretkeybytes()];
        secretBoxKey = Arrays.copyOfRange(pmalbKeyBytes, Sodium.crypto_sign_secretkeybytes(), pmalbKeyBytes.length);
    }

    public byte[] getBoxKey() {
        return secretBoxKey;
    }

    public byte[] getSignKey() {
        return secretSignKey;
    }

    public byte[] getBytes() {
        byte[] result = Arrays.copyOf(secretSignKey, secretSignKey.length + secretBoxKey.length);
        System.arraycopy(secretBoxKey, 0, result, secretSignKey.length, secretBoxKey.length);
        return result;
    }
}
