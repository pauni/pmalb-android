package org.ronidea.pmalb.core;

import android.os.Bundle;
import com.journeyapps.barcodescanner.CaptureActivity;

public class QRCodeActivity extends CaptureActivity {
    private static long latestSecretNumber = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public static long getLatestSecretNumber() {
        return latestSecretNumber;
    }

    static void setLatestSecretNumber(long latestSecretNumber) {
        QRCodeActivity.latestSecretNumber = latestSecretNumber;
    }
}