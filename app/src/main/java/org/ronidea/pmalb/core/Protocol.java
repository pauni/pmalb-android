package org.ronidea.pmalb.core;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.ronidea.pmalb.core.models.Device;
import org.ronidea.pmalb.core.models.DeviceManager;
import org.ronidea.pmalb.core.models.PmalbPacket;
import org.ronidea.pmalb.core.ui.PairingResultActivity;
import org.ronidea.pmalb.core.utilities.Cryptography;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class Protocol {
    /**
     * Tread shorts as unsigned bytes, 0-255. All above is rounded to 255
     */
    // HEADER
    public static final int  HEADER_LEN = 12;
    public static final byte VERSION   = 0;
    public static final int  PORT = 4422;
    public static final String MULTICAST_GROUP = "ff02::dead:d1cc:fab";
    // type TODO: use int instead of short and only use first two bytes, as unsigned short
    public static final short TYPE_DISCO_BEACON = 0;
    public static final short TYPE_PAIRING = 1;
    public static final short TYPE_LINK      = 3;
    public static final short TYPE_MOUSE     = 4;
    public static final short TYPE_KEYBOARD  = 5;

    private static final short SUPER_TYPE_TXT = -1;
    private static final short SUPER_TYPE_BIN = -2;
    // flags
    public static final short FLAG_BINARY   = 1;


    // UDP flags
    public static final short UDP_FLAG_SYMMETRIC   = 0b00000001;
    public static final short UDP_FLAG_ASYMMETRIC  = 0b00000011;
    public static final short UDP_FLAG_UNENCRYPTED = 0;
    public static final short UDP_FLAG_MULTICAST   = 0b00000100;
    public static final short UDP_FLAG_UNICAST     = 0;
    public static final short UDP_FLAG_NONE        = 0;


    // JSON KEYS
    public static final String DEVICE_ID     = "device_id";
    public static final String DEVICE_NAME   = "device_name";
    public static final String DEVICE_PUBKEY = "device_pubkey";
    public static final String PAIRING_STEP  = "pairing_step";
    public static final String SECRET_NUMBER = "secret_number";


    /**
     *      HEADER TCP [12 bytes]:
     *      0              8              16                             32
     *      ┌─────────────────────────────────────────────────────────────┐
     *    0 | payload size                                                |
     *    4 |                                                             |
     *      ├──────────────┬──────────────┬───────────────────────────────┤
     *    8 | Version      | Flags        | Type                          |
     *      ├──────────────┴──────────────┴───────────────────────────────┤
     *      |                          Payload                            |
     *      +-------------------------------------------------------------+
     *
     *
     *      HEADER UDP [4 bytes]:
     *      0               8              16                            32
     *      ┌───────────────┬──────────────┬──────────────────────────────┐
     *    0 | Version       | Flags        | Type                         |
     *      ├───────────────┴──────────────┴──────────────────────────────┤
     *    4 |                          Payload                            |
     *      +-------------------------------------------------------------+
     */



    // TODO: rename to processPmalbPacket, after using this method for both TCP and UDP
    static void processPmalbPacket(PmalbPacket packet, Context context) {
        switch (packet.type) {
            case TYPE_DISCO_BEACON:
                break;

            case TYPE_LINK:
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(new String(packet.data))));
                break;
            case TYPE_PAIRING:
                handlePairingResponse(packet, context);
        }
    }

    private static void handlePairingResponse(PmalbPacket packet, Context context) {
        Intent result = new Intent().setAction(PairingResultActivity.BC_PAIRING_RES);
        JSONObject pairingRequest;
        long secretNumber;
        int pairing_step;

        if (!PmalbTransceiver.isPairingMode()) {
            return;
        }

        try {
            pairingRequest = new JSONObject(new String(packet.data));
            secretNumber = pairingRequest.getLong(SECRET_NUMBER);
            pairing_step = pairingRequest.getInt(PAIRING_STEP);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        if (QRCodeActivity.getLatestSecretNumber() != secretNumber) {
            Log.w("processPmalbPacket", "Incorrect secret number!");
            return;
        }

        if (2 != pairing_step) {
            Log.w("processPmalbPacket", "Incorrect pairing step. I only receive responses");
            return;
        }

        try {
            DeviceManager.Paired.add(new Device(pairingRequest, packet.ipAddr));
            result.putExtra(PairingResultActivity.BC_PAIRING_RES, true); // set result
        } catch (JSONException e) {
            e.printStackTrace();
            result.putExtra(PairingResultActivity.BC_PAIRING_RES, false); // set result
        }

        // broadcast result
        context.sendBroadcast(result);
    }

    /**
     * Creates disco beacon to tell other Pmalb devices about it's existence
     * @return Signature of public PmalbKey, followed by the key itself.
     */
    static byte[] createDiscoBeacon() {
        byte[] msg = DeviceManager.Self.getPPK().getBytes();
        byte[] sig = Cryptography.sign(msg);

        byte[] result = Arrays.copyOf(sig, sig.length + msg.length);
        System.arraycopy(msg, 0, result, sig.length, msg.length);
        return result;
    }

    static byte[] createPairingRequest(long secretNumber) {
        JSONObject jsonPairing = DeviceManager.Self.toJson();

        if (jsonPairing == null) {
            Log.wtf("Pairing", new Throwable("Pairing failed, Self.toJson() returned Null! I'm nothing"));
        } else try {
            jsonPairing.put(PAIRING_STEP, 1);
            jsonPairing.put(SECRET_NUMBER, secretNumber);
            return jsonPairing.toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * De-/serialization UDP/TCP
     */
    static byte[] serializeForTcp(Object data, short type) {
        byte[] serialized = new byte[0];
        byte flags = 0;
        byte[] dataBytes = new byte[0];

        switch (type) {
            case TYPE_PAIRING:
                dataBytes = (byte[]) data;
                break;

            case TYPE_LINK:
            case TYPE_MOUSE:
            case SUPER_TYPE_TXT:
                dataBytes = ((String) data).getBytes();
                break;

            case SUPER_TYPE_BIN:
                flags |= FLAG_BINARY;
                break;
        }

        Log.i("serializeForTcp()", "serializing: \n" + dataBytes.toString());


        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            outputStream.write(lotoBytes(dataBytes.length));
            outputStream.write(VERSION);
            outputStream.write(flags);
            outputStream.write(ByteBuffer.allocate(2).putShort(type).array());
            outputStream.write(dataBytes);
            serialized = outputStream.toByteArray();
        } catch (IOException e) { e.printStackTrace(); }

        return serialized;
    }

    static PmalbPacket deserializeFromTcp(byte[] header) throws ArrayIndexOutOfBoundsException {
        PmalbPacket pp = new PmalbPacket();
        pp.payloadSize = bytesToLong(Arrays.copyOfRange(header, 0, 7));
        pp.version = header[8];
        pp.setFlags(header[9]);
        pp.type = bytesToShort(Arrays.copyOfRange(header, 10, 11));
        return pp;
    }

    static byte[] serializeForUdp(Object data, short type, int flags) {
        byte[] dataBytes = new byte[0];

        switch (type) {
            // all these types are text based
            case TYPE_MOUSE:
            case SUPER_TYPE_TXT:
                dataBytes = ((String) data).getBytes();
                break;

            case TYPE_DISCO_BEACON:
                dataBytes = (byte[])data;
                break;
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            outputStream.write(VERSION);
            outputStream.write((byte) (flags & 0xff));
            outputStream.write(shtoBytes(type));
            outputStream.write(dataBytes);
            return outputStream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    static PmalbPacket deserializeFromUdp(byte[] input) throws ArrayIndexOutOfBoundsException {
        PmalbPacket packet = new PmalbPacket();

        // includes header parts. Needs rename
        byte[] data = Arrays.copyOfRange(input, 2, input.length); // the rest (sometimes unencrypted)

        packet.version = input[0];
        packet.setFlags(input[1]);

        if (packet.encryptionType != 0)
            // TODO: data = Cryptography.decrypt(data, null, null);

        if (data == null) {
            Log.e("deserializeQPacket", "data == null");
            return null;
        }

        packet.type = bytesToShort(Arrays.copyOfRange(data, 0, 2)) ;
        packet.data = Arrays.copyOfRange(data, 2, data.length);

        Log.d("deserializeFromUdp", "Version: " + packet.version
                + ", encryption: " + packet.encryptionType + ", multicasted: " + packet.isMulticast
                + ", type: "   + packet.type + ", data length: " + packet.data.length);

        return packet;
    }

    // Thanks to "Wytze" for this code https://stackoverflow.com/a/29132118/8008892
    static byte[] lotoBytes(long l) {
        byte[] result = new byte[8];
        for (int i = 0; i <= 0; i++) {
            result[i] = (byte)(l & 0xFF);
            l >>= 8;
        }
        return result;
    }

    static byte[] shtoBytes(short s) {
        byte[] result = new byte[2];
        result[0] = (byte) (s & 0xff);
        s >>= 8;
        result[1] = (byte) (s & 0xff);
        return  result;
    }

    // call this if your bytes are too long
    static long bytesToLong(byte[] b) {
        long result = 0;
        for (int i = 7; i >= 0; i--) {
            result <<= 8;
            result |= (b[i] & 0xFF);
        }
        return result;
    }

    static long bytesToInt(byte[] b) {
        long result = 0;
        for (int i = 3; i >= 0; i--) {
            result <<= 8;
            result |= (b[i] & 0xFF);
        }
        return result;
    }

    // call this if your bytes are too short
    static short bytesToShort(byte[] b) {
        // shifty, shifty
        short result = 0; // bit bot
        result |= (b[1] & 0xFF);
        result <<= 8; // bity bity bit
        result |= (b[0] & 0xFF); // bit bit
        return result; // shiftly bit
    }
}

