package org.ronidea.pmalb.utilities;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.libsodium.jni.Sodium;
import org.ronidea.pmalb.core.models.DeviceManager;
import org.ronidea.pmalb.core.models.PublicPmalbKey;
import org.ronidea.pmalb.core.models.SecretPmalbKey;
import org.ronidea.pmalb.core.utilities.Cryptography;
import org.ronidea.pmalb.core.utilities.Prefs;
import org.ronidea.pmalb.core.utilities.Utils;


import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@RunWith(AndroidJUnit4.class)
public class CryptographyTest {
    private final byte[] msg = "pass me a link bro!".getBytes();
    private final Context context = InstrumentationRegistry.getTargetContext();

    @Test
    public void generateKeys() {
        DeviceManager.Self.resetKeys("test");
        Object[] keys = Cryptography.generateKeypair();
        PublicPmalbKey pub = (PublicPmalbKey) keys[0];
        SecretPmalbKey sec = (SecretPmalbKey) keys[1];

        // Assert keys not null
        assertNotNull(pub);
        assertNotNull(sec);

        // public-box-key and public-sign-key. Assert not zero
        assertNotEquals(new byte[Sodium.crypto_box_publickeybytes()], pub.getBoxKey());
        assertNotEquals(new byte[Sodium.crypto_sign_publickeybytes()], pub.getSignKey());
        // secret-box-key and secret-sign-key. Assert not zero
        assertNotEquals(new byte[Sodium.crypto_box_secretkeybytes()], sec.getBoxKey());
        assertNotEquals(new byte[Sodium.crypto_sign_secretkeybytes()], sec.getSignKey());
    }

    @Test
    public void signAndVerify() {
        DeviceManager.Self.resetKeys("test");
        Prefs.init(context);
        DeviceManager.init(context);
        PublicPmalbKey key = DeviceManager.Self.getPPK();

        byte[] sig = Cryptography.sign(msg);
        Log.i("Cryptotest", Utils.bytohex(key.getSignKey()));
        assertNotNull(key);
        assertTrue(Cryptography.verify(sig, msg, key));
        //Log.i("CryptoTest", MainActivity.bytohex(DeviceManager.Self.getSPK().getSignKey()));
    }

    @Test
    public void encryptAndDecrypt() {
        Prefs.init(context);
        DeviceManager.Self.resetKeys("test");
        DeviceManager.init(context);
        byte[] cipher = Cryptography.encrypt(msg, DeviceManager.Self.getPPK(), new byte[Sodium.crypto_box_noncebytes()]);
        Cryptography.decrypt(cipher, msg.length, DeviceManager.Self.getPPK(), new byte[Sodium.crypto_box_noncebytes()]);
    }


    @Test
    public void composition() {
        DeviceManager.Self.resetKeys("test");
        DeviceManager.init(context);

        byte[] sig = Cryptography.sign(msg);
        assertTrue(Cryptography.verify(sig, msg, DeviceManager.Self.getPPK()));

        byte[] cipher = Cryptography.encrypt(msg, DeviceManager.Self.getPPK(), new byte[Sodium.crypto_box_noncebytes()]);
        byte[] clear = Cryptography.decrypt(cipher, msg.length, DeviceManager.Self.getPPK(), new byte[Sodium.crypto_box_noncebytes()]);
        assertEquals(new String(msg), new String(clear));
    }
}
