package org.ronidea.pmalb.core.models;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.libsodium.jni.NaCl;
import org.libsodium.jni.Sodium;
import org.ronidea.pmalb.core.MainActivity;


import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@RunWith(AndroidJUnit4.class)
public class PmalbKeyTest {
    private final byte[] msg = "pass me a link bro!".getBytes();
    private final Context context = InstrumentationRegistry.getTargetContext();
    private static Sodium sodium = NaCl.sodium();

    private byte[] box;
    private byte[] sig;

    public PmalbKeyTest() {
        box = new byte[Sodium.crypto_box_publickeybytes()];
        sig = new byte[Sodium.crypto_sign_publickeybytes()];
        Arrays.fill(box, (byte) 13);
        Arrays.fill(sig, (byte) 37);
    }


    @Test
    public void constructFromTwoKeys() {
        PublicPmalbKey pub = new PublicPmalbKey(box, sig);
        assertEquals(box, pub.getBoxKey());
        assertEquals(sig, pub.getSignKey());
    }

    @Test
    public void constructFromPmalbKeyBytes() {
        byte[] keybytes = Arrays.copyOf(sig, sig.length + box.length);
        System.arraycopy(box, 0, keybytes, sig.length, box.length);
        PublicPmalbKey key = new PublicPmalbKey(keybytes);

        assertArrayEquals(box, key.getBoxKey());
        assertArrayEquals(sig, key.getSignKey());
    }
}










